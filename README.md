# yaqd-andor

[![PyPI](https://img.shields.io/pypi/v/yaq-andor)](https://pypi.org/project/yaqd-andor)
[![Conda](https://img.shields.io/conda/vn/conda-forge/yaq-andor)](https://anaconda.org/conda-forge/yaqd-andor)
[![yaq](https://img.shields.io/badge/framework-yaq-orange)](https://yaq.fyi/)
[![black](https://img.shields.io/badge/code--style-black-black)](https://black.readthedocs.io/)
[![ver](https://img.shields.io/badge/calver-YYYY.0M.MICRO-blue)](https://calver.org/)
[![log](https://img.shields.io/badge/change-log-informational)](https://gitlab.com/yaq/yaqd-andor/-/blob/master/CHANGELOG.md)

yaq daemon for andor cameras

This package contains the following daemon(s):

- https://yaq.fyi/daemons/andor-cmos

The Python SDK for the Andor CMOS can be found here:

- https://www.dropbox.com/home/KM%20JILA%20Team%20Folder/Software%20-%20Device%20Interfaces/Andor_SDKs/Python
