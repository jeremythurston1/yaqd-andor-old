__all__ = ["AndorCmos"]

import asyncio
import pathlib
import logging
import traceback
import time
from typing import Dict, Any, List
try:
    from pyAndorSDK3 import AndorSDK3
    from pyAndorSDK3 import Acquisition
except ImportError as e:
    print(f'Could not import pyAndorSDK3: {e}')
import numpy as np

from yaqd_core import Sensor

class AndorCmos(Sensor):
    _kind = "andor-cmos"

    def __init__(self, name: str, config: Dict[str, Any], config_filepath: pathlib.Path):
        super().__init__(name, config, config_filepath)

        # logging.error(self._measured)

        logging.error("Connecting to camera...")
        self.sdk3 = AndorSDK3()
        self.camera = self.sdk3.GetCamera(0)
        logging.error(f"Connected to camera {self.camera.SerialNumber}")

        self.camera.AOIHeight = config['aoi_height']
        self.camera.AOIWidth = config['aoi_width']
        self.camera.ExposureTime = config['exposure_time']
        self.timeout = self.camera.ExposureTime * config['timeout_ratio']

        self._channel_names = ["image"]
        self._channel_units = {"image": "pixels"}
        self._channel_shapes = {"image": (self.camera.AOIHeight, self.camera.AOIWidth)}

        # raise('TEST TEST TEST TEST')

    # def currently_acquiring(self) -> bool:
    #     """Returns true if the camera is currently in the process of acquiring an image"""

    # def process_image(self, acquisition) -> float:
    #     """returns a np array from the Acquisition object generated from camera._acquire"""
    #     return acquisition.image.astype(np.float32)

    # def get_image(self):
    #     # Set a flag like self.busy = true
    #     """Returns an individual image."""
    #     # not needed
    #     # self.camera.AcquisitionStart()
    #     acq = self.camera._acquire(self.timeout)
    #     acq = self.process_image(acq)
    #     # self.camera.AcquisitionStop()
    #     self.camera._flush()
    #     return acq

    def get_measured(self):
        try:
            return super().get_measured()
        except Exception as e:
            traceback.print_stack()
            raise

    async def _measure(self) -> float:
        # """Takes a measurement"""
        raise('TEST TEST TEST TEST TEST TEST TEST TEST ')
        # logging.error('IS IT GETTING TO _MEASURE?')
        # acq = self.camera.acquire(timeout=self.timeout)
        # acq = acq.image.astype(np.float32)
        # acq = {'image': 1.1}

        # out = {}

        # acq = np.asarray([1.0, 2.0, 3.0])
        # f = open('C:\\Users\\KM\\Documents\\PythonSoftware\\foo.txt')
        # f.write(str(acq))
        # f.close()

        # out['image'] = acq
        # logging.error(acq)

        # logging.error('IS IT GETTING TO _MEASURE2?')
        # raise(acq)
        # results = {'image': acq, "measurement_id": time.time()}
        # raise('is it getting here lol###############')
        # return 7.3
        # return out
        # return acq
        return {"image": 1.1}
        # return {'image': np.asarray([0.0, 1.0, 2.0])}

    # async def update_state(self) -> None:
    #     """Continually monitor and update the current daemon state."""
    #     while True:
    #         # self._busy = False
    #         # if self.AcquisitionStart():
    #         #     self._busy = True
    #         # elif self.AcquisitionStop():
    #         #     self._busy = False

    #         # Start an acquisition
    #         await self._measure()

    #         # logging.error('UPDATING STATE!@!@!@!@!@')

    #         if self._busy:
    #             try:
    #                 await asyncio.sleep(0.01)
    #             except asyncio.CancelledError:
    #                 logging.error("Update_state cancelled")
    #         else:
    #             await self._busy_sig.wait()

if __name__ == "__main__":
    # raise('HELLO HELLO HELLO')
    AndorCmos.main()
